package cn.java.test;

import java.util.List;

import cn.yunhe.entity.Author;
import cn.yunhe.entity.Publish;
import org.json.JSONObject;
import org.junit.Test;

import cn.yunhe.entity.Book;
import cn.yunhe.service.IBookService;
import cn.yunhe.service.Impl.BookServImpl;

public class BookTest {

	IBookService bsi = new BookServImpl();
	@Test
	public void bookTest(){
		List<Book> list = bsi.getAllBook();
		
		System.out.println(list);
		for (Book book : list) {
			System.out.println(book);
			
		}
		String listString = JSONObject.valueToString(list);
		System.out.println(listString);
	}

	@Test
	public void addBookTest(){
		IBookService bookService = new BookServImpl();
		Book book = new Book("No.001","西游记",new Author(18001),new Publish(93101),72,81,null);
		boolean result = bookService.addBook(book);
		System.out.println(result);
	}
}
