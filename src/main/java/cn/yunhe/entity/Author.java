package cn.yunhe.entity;

/**
 * @Auther： LiShen Wang
 * @Description： 作者表的实体类 作者属性有：作者id 作者名称 作者性别
 * @Date： Created by 11:43  2018/4/13.
 */
public class Author {
    private int authorId;//作者id
    private String authorName;//作者名称
    private int authorSex;//作者性别

    public Author() {
    }

    public Author(int authorId) {
        this.authorId = authorId;
    }

    public Author(int authorId, String authorName, int authorSex) {
        this.authorId = authorId;
        this.authorName = authorName;
        this.authorSex = authorSex;
    }

    public String toString() {
        return "Author{" +
                "authorId=" + authorId +
                ", authorName='" + authorName + '\'' +
                ", authorSex=" + authorSex +
                '}';
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public int getAuthorSex() {
        return authorSex;
    }

    public void setAuthorSex(int authorSex) {
        this.authorSex = authorSex;
    }
}
