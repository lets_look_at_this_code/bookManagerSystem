package cn.yunhe.entity;

/**
 * @Auther： LiShen Wang
 * @Description： 出版社表的实体类
 * @Date： Created by 11:46  2018/4/13.
 */
public class Publish {
    private int publishId;//出版社id
    private String publishName;//出版社名称
    private String address;//出版社地址

    public Publish() {
    }

    public Publish(int publishId) {
        this.publishId = publishId;
    }

    public Publish(int publishId, String publishName, String address) {
        this.publishId = publishId;
        this.publishName = publishName;
        this.address = address;
    }

    public int getPublishId() {
        return publishId;
    }

    public void setPublishId(int publishId) {
        this.publishId = publishId;
    }

    public String getPublishName() {
        return publishName;
    }

    public void setPublishName(String publishName) {
        this.publishName = publishName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String toString() {
        return "Publish{" +
                "publishId=" + publishId +
                ", publishName='" + publishName + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
