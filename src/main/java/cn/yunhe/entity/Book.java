package cn.yunhe.entity;

/**
 * @Auther： LiShen Wang
 * @Description： 图书表实体类，描述图书表的列
 * @Date： Created by 10:57  2018/4/13.
 */
public class Book {
    private String isbn;//图书编号
    private String bookName;//图书名称
    private Author author;//作者
    private Publish publish;//出版社
    private double price;//售价
    private int currCount;//当前库存
    private String picture;//图书图片

    public Book() {
    }

    public Book(String isbn, String bookName, Author author, Publish publish, double price, int currCount, String picture) {
        this.isbn = isbn;
        this.bookName = bookName;
        this.author = author;
        this.publish = publish;
        this.price = price;
        this.currCount = currCount;
        this.picture = picture;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Publish getPublish() {
        return publish;
    }

    public void setPublish(Publish publish) {
        this.publish = publish;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCurrCount() {
        return currCount;
    }

    public void setCurrCount(int currCount) {
        this.currCount = currCount;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String toString() {
        return "Book{" +
                "isbn='" + isbn + '\'' +
                ", bookName='" + bookName + '\'' +
                ", author=" + author +
                ", publish=" + publish +
                ", price=" + price +
                ", currCount=" + currCount +
                ", picture='" + picture + '\'' +
                '}';
    }
}
