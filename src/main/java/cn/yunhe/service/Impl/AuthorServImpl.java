package cn.yunhe.service.Impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import cn.yunhe.dao.IAuthorDao;
import cn.yunhe.entity.Author;
import cn.yunhe.service.IAuthorService;
import cn.yunhe.util.SqlSessionFactoryUtil;

public class AuthorServImpl implements IAuthorService {

	@Override
	public List<Author> getAllAuthor() {
		//定义变量接受从数据库中查询到的数据，用于返回
		List<Author> list = null;
		//得到SqlSession 
		SqlSession session = SqlSessionFactoryUtil.getSession();
		try {
			//通过Session获取dao层接口的对象
			IAuthorDao authorDao = session.getMapper(IAuthorDao.class);
			list = authorDao.getAllAuthor();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			session.close();
		}
		return list;
	}

}
