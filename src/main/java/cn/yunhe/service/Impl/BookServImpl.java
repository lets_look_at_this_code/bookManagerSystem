package cn.yunhe.service.Impl;

import java.util.List;

import cn.yunhe.dao.IBookDao;
import cn.yunhe.entity.Book;
import cn.yunhe.service.IBookService;
import cn.yunhe.util.SqlSessionFactoryUtil;

import org.apache.ibatis.session.SqlSession;

/**
 * @Auther： LiShen Wang
 * @Description：
 * @Date： Created by 14:55  2018/4/13.
 */
public class BookServImpl implements IBookService {

    public boolean addBook(Book book) {
        //定义一个标记变量flag 用于返回添加结果
        boolean flag = false;
        //得到session
        SqlSession session = SqlSessionFactoryUtil.getSession();
        try {
            //得到 Book的 dao层对象
            IBookDao bookDao = session.getMapper(IBookDao.class);
            //利用bookDao 完成添加操作
            int result = bookDao.addBook(book);
            //数据库的增删改操作需要提交
            if(result>0){
                session.commit();
                flag = true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
        return flag;
    }

	public List<Book> getAllBook() {
		//定义一个List<Book> 用于返回查询结果
		List<Book> list = null;
        //得到session
        SqlSession session = SqlSessionFactoryUtil.getSession();
        try {
            //得到 Book的 dao层对象
            IBookDao bookDao = session.getMapper(IBookDao.class);
            //利用bookDao 完成查询操作
            list = bookDao.getAllBook();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
		return list;
	}
}
