package cn.yunhe.service;

import java.util.List;

import cn.yunhe.entity.Book;

/**
 * @Auther： LiShen Wang
 * @Description： 操作dao层Book元素给数据库中Book表做修改
 * @Date： Created by 14:52  2018/4/13.
 */
public interface IBookService {
    /**
     * @Description: 
     * @Param: 一个book图书对象
     * @Return: 是否添加成功
     */
    public boolean addBook(Book book);
    
    /**
     * 查询所有图书信息，未完成分页
     * @return
     */
    public List<Book> getAllBook();

}
