package cn.yunhe.service;

import java.util.List;

import cn.yunhe.entity.Author;

public interface IAuthorService {
	
	public List<Author> getAllAuthor();
}
