package cn.yunhe.web.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.json.JSONArray;
import org.json.JSONObject;

import cn.yunhe.entity.Book;
import cn.yunhe.service.IBookService;
import cn.yunhe.service.Impl.BookServImpl;

/**
 * @Auther： LiShen Wang
 * @Description： 图书操作的Action类
 * @Date： Created by 16:19 2018/4/13.
 */
public class BookAction extends BaseAction {
	private Book book;

	private File picture;
	private String pictureFileName;
	private String pictureContentType;

	private String savePath;
	IBookService bookService = new BookServImpl();

	/**
	 * @Description: 类似于 执行文件上传功能的工具类方法
	 * @Param: 要上传的文件，上传的存放路径
	 */
	private void doUpload(File file, String uploadFilePath) throws IOException {
		FileInputStream fis = new FileInputStream(file);
		FileOutputStream fos = new FileOutputStream(uploadFilePath);
		byte[] buffer = new byte[1024];
		int len = 0;
		while ((len = fis.read(buffer)) != -1) {
			fos.write(buffer, 0, len);
		}
		fis.close();
		fos.close();
	}

	public String add() throws IOException {

		if (picture != null) {
			String uploadPath = getSavePath() + "\\" + getPictureFileName();
			doUpload(picture, uploadPath);
			String[] str = uploadPath.split("\\\\");
			String picturePath = str[str.length-2]+"\\"+str[str.length-1];
			System.out.println(picturePath);
			book.setPicture(picturePath);
		}
		boolean result = bookService.addBook(book);
		if (result) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}


	public File getPicture() {
		return picture;
	}

	public void setPicture(File picture) {
		this.picture = picture;
	}

	public String getPictureFileName() {
		return pictureFileName;
	}

	public void setPictureFileName(String pictureFileName) {
		this.pictureFileName = pictureFileName;
	}

	public String getPictureContentType() {
		return pictureContentType;
	}

	public void setPictureContentType(String pictureContentType) {
		this.pictureContentType = pictureContentType;
	}

	/**
	 * @Description: 修改了默认get方法 ，
	 * @Return: 图片存储路径的真实路径
	 */
	public String getSavePath() {
		return ServletActionContext.getServletContext().getRealPath(savePath);
	}

	public void setSavePath(String savePath) {
		this.savePath = savePath;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}
}
