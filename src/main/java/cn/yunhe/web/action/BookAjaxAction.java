package cn.yunhe.web.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.json.JSONObject;

import cn.yunhe.entity.Book;
import cn.yunhe.service.IBookService;
import cn.yunhe.service.Impl.BookServImpl;

public class BookAjaxAction extends BaseAction {
	private String returnDate;

	IBookService bsi = new BookServImpl();
	public String selectAll() throws IOException {
		List<Book> list = bsi.getAllBook();
		String listString = JSONObject.valueToString(list);
		returnDate = listString;
		return SUCCESS;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}
}
