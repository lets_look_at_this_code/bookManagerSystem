package cn.yunhe.web.action;

import java.io.IOException;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.opensymphony.xwork2.ModelDriven;

import cn.yunhe.entity.Author;
import cn.yunhe.service.IAuthorService;
import cn.yunhe.service.Impl.AuthorServImpl;

public class AuthorAction extends BaseAction implements ModelDriven<Author>{
	private Author author= new Author();
	
	IAuthorService asi = new AuthorServImpl();
	public void ajaxSelect() throws IOException{
		List<Author> list = asi.getAllAuthor();
		String jsonList = JSONObject.valueToString(list);
		getResponse().getWriter().print(jsonList);
	}
	
	public Author getModel() {
		return author;
	}
	
}
