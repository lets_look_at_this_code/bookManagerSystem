package cn.yunhe.web.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by Administrator on 2018/4/3.
 */
public class BaseAction extends ActionSupport{

    public Map<String, Object> getMapRequest() {
        return (Map<String, Object>) ActionContext.getContext().get("request");
    }

    public Map<String, Object> getMapSession() {
        return ActionContext.getContext().getSession();
    }

    public HttpServletRequest getRequest() {
        HttpServletRequest request =ServletActionContext.getRequest();
        try {
            request.setCharacterEncoding("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return request;
    }

    public HttpSession getSession() {
        return ServletActionContext.getRequest().getSession();
    }

    public HttpServletResponse getResponse() {
        HttpServletResponse response = ServletActionContext.getResponse();
        response.setContentType("text/html;charset=UTF-8");
        return response;
    }


}
