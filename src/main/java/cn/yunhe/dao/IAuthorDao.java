package cn.yunhe.dao;

import java.util.List;

import cn.yunhe.entity.Author;

/**
 * 作者表的dao层接口
 * @author WLS
 */
public interface IAuthorDao {
	/**
	 * 得到所有的作者信息
	 * @return 
	 */
	public List<Author> getAllAuthor();
	
}
