package cn.yunhe.dao;

import java.util.List;

import cn.yunhe.entity.Book;

/**
 * @Auther： LiShen Wang
 * @Description：
 * @Date： Created by 14:34  2018/4/13.
 */
public interface IBookDao {
    /**
     * @Description: 
     * @Param: 书籍对象
     * @Return: 数据库受影响的行数 添加成功=1 失败=0
     */
    public int addBook(Book book);
    
    /**
     * 查询所有书籍信息，没完成分页功能
     * @return
     */
    public List<Book> getAllBook();
    
    
}
