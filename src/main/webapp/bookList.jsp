<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
.head {
	width: 800px;
	text-align: center;
	margin: 0 auto;
	margin-top: 50px;
}

.splitpage {
	width: 800px;
	margin: 0 auto;
}

ul {
	list-style: none;
}

li {
	width: 40px;
	height: 40px;
	border: 1px solid silver;
	float: left;
	line-height: 40px;
	text-align: center;
	font-size: 10px;
}

td {
	width: 100px;
	text-align: center;
}

.picture {
	text-align: center;
	width: 200px;
}
</style>

<title>图书列表</title>
</head>
<body>
	<div class="head">
		<h2>图书管理系统</h2>
		<div id="search">
			<input type="text" name="keyword" value="" /><span><input
				type="button" value="搜索" onclick="sel()" /></span>
		</div>
		<div>
			<a href="bookAdd.jsp">添加图书</a>
		</div>
	</div>
	<table align="center" border="1" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<td>图书编号</td>
				<td>图书名字</td>
				<td>图书作者</td>
				<td>出版社</td>
				<td>图书价格</td>
				<td>图书库存</td>
				<td class="picture">图书图片</td>
				<td>操作</td>
			</tr>
		</thead>
		<tbody id="tab">

		</tbody>
	</table>
	<div class="splitpage">
		<ul>
			<li>首页</li>
			<li>上一页</li>
			<li>1</li>
			<li>2</li>
			<li>3</li>
			<li>下一页</li>
			<li>尾页</li>
		</ul>
	</div>
	<script type="text/javascript" src="jquery/jquery-1.8.3.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$.ajax({
				type : 'post',
				url : 'bookselectAll',
				success : function(data) {
					//对返回的数据新型处理
					var lists = JSON.parse(data);//将结果字符串解析成集合对象
					var tbody = "";//创建空的列表节点，用以向table中添加
					$.each(lists,function(i, book) {//用jQuery的each循环遍历数组，
						tbody += "<tr><td>"+book.isbn+"</td><td>"
								+ book.bookName+"</td><td>"
								+ book.author.authorName+"</td><td>"
								+ book.publish.publishName+"</td><td>"
								+ book.price+"</td><td>"
								+ book.currCount+"</td>"
								+ "<td><img alt='我看看你是啥' src="+book.picture+" /></td>"
								+ "<td><a href='#'>删除</a></td></tr>";
					});
					$("#tab").append(tbody);

				}
			});
		});
	</script>
</body>
</html>