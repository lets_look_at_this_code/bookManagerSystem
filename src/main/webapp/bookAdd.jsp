<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>添加图书</title>
<script type="text/javascript" src="jquery/jquery-1.8.3.js"></script>
<script type="text/javascript">
	$(function() {
		$.ajax({
			type : 'post',
			url : "ajaxSelectAuthor",
			success : function(data) {
				//对返回的数据新型处理
				var authors = JSON.parse(data);//将字符串解析为json数组
				
				var option = "<option value=''>请选择作者</option>";//填写下拉列表的默认选项

				$.each(authors, function(i, author) {//用jQuery的each循环遍历数组，
					//将下拉列表的选项 追加到option后面
					option += "<option value="+author.authorId+">"
							+ author.authorName + "</option>";
				});
				$("#author").append(option);//给下拉列表追加节点
			}
		});
	});
</script>
</head>
<body>
	<s:fielderror />
	<table align="center">
		<s:form action="addBook" method="post" enctype="multipart/form-data">
			<tr>
				<td>ISBN:</td>
				<td><s:textfield name="book.isbn" label="" /><span></span></td>
			</tr>
			<tr>
				<td>图书名字:</td>
				<td><s:textfield name="book.bookName" label="" /><span></span></td>
			</tr>
			<tr>
				<td>图书作者:</td>
				<td><select id="author" name="book.author.authorId"></select><span></span></td>
			</tr>
			<tr>
				<td>出版社:</td>
				<td><s:textfield name="book.publish.publishId" label="" /><span></span></td>
			</tr>
			<tr>
				<td>售价:</td>
				<td><s:textfield name="book.price" label="" /><span></span></td>
			</tr>
			<tr>
				<td>入库数量:</td>
				<td><s:textfield name="book.currCount" label="" /><span></span></td>
			</tr>
			<tr>
				<td>图书图片:</td>
				<td><s:file name="picture" label="" /><span></span></td>
			</tr>
			<tr>
				<td></td>
				<td><s:submit value="添加" /></td>
			</tr>
		</s:form>
	</table>
</body>
</html>
